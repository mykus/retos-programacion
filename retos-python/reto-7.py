# Reto #7
# CONTANDO PALABRAS
# Fecha publicación enunciado: 14/02/22
# Fecha publicación resolución: 21/02/22
# Dificultad: MEDIA
#
# Enunciado: Crea un programa que cuente cuantas veces se repite cada palabra y que muestre el recuento final de todas ellas.
# - Los signos de puntuación no forman parte de la palabra.
# - Una palabra es la misma aunque aparezca en mayúsculas y minúsculas.
# - No se pueden utilizar funciones propias del lenguaje que lo resuelvan automáticamente.
#

import re

frase = "Hola, mi nombre es miguel. Mi nombre completo es Jose Miguel (Pero no me gusta)."

# Mediante este metodo se puede dividir la frase por espacio o el separador que se le de
# frase_partida = frase.split()

# Mediante este otro metodo, no solo se le quitan los espacios, sino que tambien se pueden ignorar los signos de puntuación.
frase_partida = re.findall(r"\w+", frase.lower())

recuento = {}

for palabra in frase_partida:
    existe = False
    for pal, vez in recuento.items():
        if palabra == pal:
            recuento[pal] += 1
            existe = True
    
    if not existe:
        recuento[palabra] = 1

for palabra, veces in recuento.items():
    if veces > 1:
        print("La palabra", palabra, "se repite", veces, "veces.")
    else:
        print("La palabra", palabra, "se repite", veces, "vez.")