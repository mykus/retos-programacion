#
# Reto #4
# ÁREA DE UN POLÍGONO
# Fecha publicación enunciado: 24/01/22
# Fecha publicación resolución: 31/01/22
# Dificultad: FÁCIL
#
# Enunciado: Crea UNA ÚNICA FUNCIÓN (importante que sólo sea una) que sea capaz de calcular y retornar el área de un polígono.
# - La función recibirá por parámetro sólo UN polígono a la vez.
# - Los polígonos soportados serán Triángulo, Cuadrado y Rectángulo.
# - Imprime el cálculo del área de un polígono de cada tipo.

def area_poligon(poli, dato1, dato2):
    if poli == "triangulo":
        area = (dato1 * dato2) / 2
        return area
    elif poli == "cuadrado":
        area = dato1 * dato2
        return area
    elif poli == "rectangulo":
        area = dato1 * dato2
        return area
    else:
        return "El poligono indicado no es correcto"

poligono = "triangulo"

print(area_poligon(poligono, 5, 5))