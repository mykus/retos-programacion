#
# Reto #6
# INVIRTIENDO CADENAS
# Fecha publicación enunciado: 07/02/22
# Fecha publicación resolución: 14/02/22
# Dificultad: FÁCIL
#
# Enunciado: Crea un programa que invierta el orden de una cadena de texto sin usar funciones propias del lenguaje que lo hagan de forma automática.
# - Si le pasamos "Hola mundo" nos retornaría "odnum aloH"
#

def inversion(frase):
    frase = list(frase)
    in_frase = list(range(len(frase)))
    cont = 1

    for letra in frase:
        in_frase[len(frase)-cont] = letra
        cont += 1

    in_frase = "".join(in_frase)

    return in_frase

print(inversion("Hola Mundo"))