#
 #Reto #1
 #¿ES UN ANAGRAMA?
 #Fecha publicación enunciado: 03/01/22
 #Fecha publicación resolución: 10/01/22
 #Dificultad: MEDIA
#
 #Enunciado: Escribe una función que reciba dos palabras (String) y retorne verdadero o falso (Boolean) según sean o no anagramas.
 #Un Anagrama consiste en formar una palabra reordenando TODAS las letras de otra palabra inicial.
 #NO hace falta comprobar que ambas palabras existan.
 #Dos palabras exactamente iguales no son anagrama.


from ctypes.wintypes import HLOCAL


def es_anagrama(word1, word2):
    if word1 == word2:
        return False
    elif len(word1) != len(word2):
        return False
    else:
        word1 = list(word1)
        word2 = list(word2)
        for letra1 in word1:
            for letra2 in word2:
                if letra1 == letra2:
                    word2.remove(letra2)
        if len(word2) == 0:
            return True
        else:
            return False


word1 = "roma"
word2 = "amor"

print(es_anagrama(word1, word2))