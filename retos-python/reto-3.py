#
# Reto #3
# ¿ES UN NÚMERO PRIMO?
# Fecha publicación enunciado: 17/01/22
# Fecha publicación resolución: 24/01/22
# Dificultad: MEDIA
#
# Enunciado: Escribe un programa que se encargue de comprobar si un número es o no primo.
# Hecho esto, imprime los números primos entre 1 y 100.


def es_primo(num):
    imp = False

    if num != 1:
        imp = True

        for i in range(100):
            divisor = i+1
            if (divisor != 1) and (divisor != num):
                if num%divisor == 0:
                    imp = False
    
    return imp

for i in range(100):
    num = i+1
    if es_primo(num):
        print(num)
