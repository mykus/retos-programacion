 # Reto #10
 # EXPRESIONES EQUILIBRADAS
 # Fecha publicación enunciado: 07/03/22
 # Fecha publicación resolución: 14/03/22
 # Dificultad: MEDIA
 #
 # Enunciado: Crea un programa que comprueba si los paréntesis, llaves y corchetes de una expresión están equilibrados.
 # - Equilibrado significa que estos delimitadores se abren y cieran en orden y de forma correcta.
 # - Paréntesis, llaves y corchetes son igual de prioritarios. No hay uno más importante que otro.
 # - Expresión balanceada: { [ a * ( c + d ) ] - 5 }
 # - Expresión no balanceada: { a * ( c + d ) ] - 5 }

def is_balanced(expresion):
    expresion = list(expresion)
    balance = True
    compr = False

    while(not compr and balance):
        if "{" in expresion:
            if "}" in expresion:
                expresion.remove("{")
                expresion.remove("}")
            else:
                balance = False
        
        if "(" in expresion:
            if ")" in expresion:
                expresion.remove("(")
                expresion.remove(")")
            else:
                balance = False
        
        if "[" in expresion:
            if "]" in expresion:
                expresion.remove("[")
                expresion.remove("]")
            else:
                balance = False

        if (not "{" in expresion) or (not "(" in expresion) or (not "[" in expresion):
            compr = True

    if ("}" in expresion) or (")" in expresion) or ("]" in expresion):
        balance = False
    
    return balance

if is_balanced("{ [ a * ( c + d ) ] - 5 }"):
    print("La expresion esta balanceada")
else:
    print("La expresion tiene un ERROR!!")

if is_balanced("{  a * ( c + d ) ] - 5 }"):
    print("La expresion esta balanceada")
else:
    print("La expresion tiene un ERROR!!")
