# Reto #9
# CÓDIGO MORSE
# Fecha publicación enunciado: 02/03/22
# Fecha publicación resolución: 07/03/22
# Dificultad: MEDIA
#
# Enunciado: Crea un programa que sea capaz de transformar texto natural a código morse y viceversa.
# - Debe detectar automáticamente de qué tipo se trata y realizar la conversión.
# - En morse se soporta raya "—", punto ".", un espacio " " entre letras o símbolos y dos espacios entre palabras "  ".
# - El alfabeto morse soportado será el mostrado en https://es.wikipedia.org/wiki/Código_morse.
#

# Definimos los diccionarios
letras = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", " "]
morse = [".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", "  "]

dic_wrd2mrs = dict(zip(letras, morse))
dic_mrs2wrd = dict(zip(morse, letras))


def mrs2wrd(frase):
    global dic_mrs2wrd
    frase = frase.split(" ")
    frase_tran = []
    for letra in frase:
        if letra == "":
            frase_tran.append(" ")
        else:
            frase_tran.append(dic_mrs2wrd[letra])
    frase_tran = "".join(frase_tran)
    return frase_tran
    

def wrd2mrs(frase):
    global dic_wrd2mrs
    frase = list(frase)
    frase_tran = []
    for letra in frase:
        if letra != " ":
            frase_tran.append(dic_wrd2mrs[letra])
            frase_tran.append(" ")
        else:
            frase_tran.pop(-1)
            frase_tran.append(dic_wrd2mrs[letra])

    frase_tran.pop(-1)
    frase_tran = "".join(frase_tran)
    return frase_tran

def enigma(frase):
    if "-" in frase:
        frase_tran = mrs2wrd(frase)
    else:
        frase_tran = wrd2mrs(frase)
    
    return frase_tran


print(enigma("hola mundo"))
print(enigma(".... --- .-.. .-  -- ..- -. -.. ---"))
