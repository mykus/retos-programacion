# Reto #8
# DECIMAL A BINARIO
# Fecha publicación enunciado: 18/02/22
# Fecha publicación resolución: 02/03/22
# Dificultad: FÁCIL
#
# Enunciado: Crea un programa se encargue de transformar un número decimal a binario sin utilizar funciones propias del lenguaje 
# que lo hagan directamente.
#
import math

def binario(num):
    resul = num
    bin = []
    while (num !=0):
        resul = num / 2
        resto = num % 2
        bin.append(resto)
        num = math.floor(resul)

    bin.reverse()
    bin = [str(i) for i in bin]
    bin = "".join(bin)
    bin = int(bin)
    return bin

print(binario(1))
print(binario(7))
print(binario(12))
